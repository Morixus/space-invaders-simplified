using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletControl : Score
{
    [SerializeField] private GameObject _parent;
    private Rigidbody2D[] _amountOfChildren;
    private int _remainingInColumn;

    private void Awake()
    {
        if (transform.parent != null)
        {
            _parent = transform.parent.gameObject;
            transform.SetParent(null);
            _amountOfChildren = _parent.transform.parent.GetComponentsInChildren<Rigidbody2D>();
            _remainingInColumn = _amountOfChildren.Length;
        }
    }
    void Update()
    {
        transform.position += transform.up * Time.deltaTime * 2f;
        StartCoroutine(AutoDestroy());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<EnemyShooter>() || collision.GetComponent<EnemyTanker>())
        {
            _score++;
            Debug.Log(_score);
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        else if (collision.GetComponent<PlayerController>())
        {
            TakeDamage();
            Destroy(this.gameObject);
        }
    }

    public void TakeDamage()
    {
        _score -= _remainingInColumn * 2;
    }

    IEnumerator AutoDestroy()
    {
        yield return new WaitForSeconds(2.5f);
        Destroy(this.gameObject);
    }
}