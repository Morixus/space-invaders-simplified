﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesMovement : MonoBehaviour
{
    [SerializeField] public float enemySpeed;
    [SerializeField] private GameManager _gameMan;
    [SerializeField] private float _fallValue = 0.5f;
    private bool _canGoDown = false;
    private bool _isChangingDir = false;
    private int _count;
    private int _children;
    private bool _canEnd = true;

    void LateUpdate()
    {
        _children = GetChildren(gameObject);
        if (_children <= 38 && _canEnd == true)
        {
            _canEnd = false;
            _gameMan.EndGame();
            enemySpeed = 0f;
        }

        if (_canGoDown)
        {
            GoDown();
        }

        if (transform.position.x >= 0.4f)
        {
            _isChangingDir = !_isChangingDir;
            _canGoDown = true;
        }
        else if (transform.position.x <= -0.4f)
        {
            _isChangingDir = !_isChangingDir;
        }

        if (!_isChangingDir)
        {
            transform.Translate(-enemySpeed, 0f, 0f);
        }
        else
        {
            transform.Translate(enemySpeed, 0f, 0f);
        }
    }

    private void GoDown()
    {
        _canGoDown = false;
        transform.Translate(0f, -_fallValue, 0f);
        Debug.Log("Going Down");
    }

    public int GetChildren(GameObject p_obj)
    {
        _count = 0;
        for (int i = 0; i < p_obj.transform.childCount; i++)
        {
            _count++;
            Counter(p_obj.transform.GetChild(i).gameObject, ref _count);
        }
        return _count;
    }

    private void Counter(GameObject p_currentObj, ref int p_count)
    {
        for (int i = 0; i < p_currentObj.transform.childCount; i++)
        {
            p_count++;
            Counter(p_currentObj.transform.GetChild(i).gameObject, ref p_count);
        }
    }
}