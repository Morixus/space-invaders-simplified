using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooter : MonoBehaviour
{
    [SerializeField] private GameObject _bulletPrefab = null;
    [SerializeField] private BulletControl _bulletScript = null;
    [SerializeField] private GameManager _gameMan = null;
    [SerializeField] private EnemiesMovement _enemies = null;
    private Vector3 _bulletSpawnPosition = new Vector3(0f, 0.5f, 0f);
    private float _shootCD;
    private float _lastShot = 0f;
    private RaycastHit2D _hit;
    private GameObject _bullet;

    private void Awake()
    {
        _shootCD = Random.Range(4, 8);
        _enemies = transform.parent.transform.parent.GetComponent<EnemiesMovement>();
    }
    private void Update()
    {
        _hit = Physics2D.Raycast(transform.position, Vector2.down, 1f);

        _lastShot += Time.deltaTime;
        if (_lastShot >= _shootCD && _hit.collider == null)
        {
            Shoot();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            _bulletScript.TakeDamage();
        }
        if (collision.GetComponent<EndCollider>())
        {
            _enemies.enemySpeed = 0f;
            _gameMan.EndGame();
        }
    }

    private void Shoot()
    {
        _bullet = Instantiate(_bulletPrefab, this.transform.position - _bulletSpawnPosition, Quaternion.Euler(0f, 0f, 180f), this.transform);
        _bullet.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        _lastShot = 0f;
        _shootCD = Random.Range(4, 8);
    }
}