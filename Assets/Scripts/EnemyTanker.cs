using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTanker : MonoBehaviour
{
    [SerializeField] private BulletControl _bullet;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            _bullet.TakeDamage();
        }
    }
}