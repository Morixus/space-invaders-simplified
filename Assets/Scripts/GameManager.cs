using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Score _score;
    [SerializeField] private GameObject _endGamePanel;
    [SerializeField] private Button _backToMenuButton;
    [SerializeField] private Highscores _highscore;
    [SerializeField] private TMP_Text _endPoints;
    [SerializeField] private GameObject _endRank;
    [SerializeField] private TMP_Text _endRankText;
    private string _jsonGames;
    private string _json;
    private PlayedGames _playedGames;
    private int _playedGamesNumber;


    private void Awake()
    {
        LoadPlayedGames();
        _playedGamesNumber = _playedGames.playedGames;
    }

    private void Start()
    {
        _backToMenuButton.onClick.AddListener(() => BackToMenu());
    }

    public void EndGame()
    {
        _playedGamesNumber++;
        Debug.Log("Your points: " + _score.finalScore);
        _endPoints.text = "Your points: " + _score.finalScore;
        _playedGames = new PlayedGames { playedGames = _playedGamesNumber };
        _highscore.AddHighscoreEntry(_score.finalScore);
        if (_highscore.currentRank <= 10 && _highscore.currentRank >= 1)
        {
            _endRank.SetActive(true);
            _endRankText.text = "Your rank: " + _highscore.currentRank;
        }
        Time.timeScale = 0f;
        _endGamePanel.SetActive(true);

        SavePlayedGames();
    }

    private void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    private void SavePlayedGames()
    {
        _json = JsonUtility.ToJson(_playedGames);
        PlayerPrefs.SetString("playedGames", _json);
        PlayerPrefs.Save();
    }

    private void LoadPlayedGames()
    {
        _jsonGames = PlayerPrefs.GetString("playedGames");
        _playedGames = JsonUtility.FromJson<PlayedGames>(_jsonGames);
    }
}