using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class Highscores : MonoBehaviour
{
    [SerializeField] private Transform _entryContainer;
    [SerializeField] private Transform _entryTemplate;
    [SerializeField] private TMP_Text _playedGamesText;
    private RectTransform _entryRectTransform;
    private Transform _entryTransform;
    private List<Transform> _highscoreEntryTransformList;
    private HighscoreEntry _tmp;
    private HighscoreEntry _highscoreEntry;
    private HighscoresList _highscores;
    private PlayedGames _playedGames;
    private float _templateHeight = 25f;
    private int _rank;
    private int _score;
    private string _json;
    private string _jsonString;
    public int currentRank;

    private void Awake()
    {
        _entryTemplate.gameObject.SetActive(false);

        _jsonString = PlayerPrefs.GetString("highscoreTable");
        _highscores = JsonUtility.FromJson<HighscoresList>(_jsonString);
        _jsonString = PlayerPrefs.GetString("playedGames");
        _playedGames = JsonUtility.FromJson<PlayedGames>(_jsonString);

        _playedGamesText.text = "Played games: " + _playedGames.playedGames;

        /*_highscores.highscoreEntryList.Clear();
        _json = JsonUtility.ToJson(_highscores);
        PlayerPrefs.SetString("highscoreTable", _json);
        PlayerPrefs.Save();

        _playedGames.playedGames = 0;
        _json = JsonUtility.ToJson(_playedGames);
        PlayerPrefs.SetString("playedGames", _json);
        PlayerPrefs.Save();*/

        for (int i = 0; i < _highscores.highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < _highscores.highscoreEntryList.Count; j++)
            {
                if (_highscores.highscoreEntryList[j].score > _highscores.highscoreEntryList[i].score)
                {
                    _tmp = _highscores.highscoreEntryList[i];
                    _highscores.highscoreEntryList[i] = _highscores.highscoreEntryList[j];
                    _highscores.highscoreEntryList[j] = _tmp;
                }
            }
        }

        _highscoreEntryTransformList = new List<Transform>();
        foreach (HighscoreEntry highscoreEntry in _highscores.highscoreEntryList)
        {
            CreateHighscoreEntryTransform(highscoreEntry, _entryContainer, _highscoreEntryTransformList);
        }
    }

    private void CreateHighscoreEntryTransform(HighscoreEntry p_highscoreEntry, Transform p_container, List<Transform> p_transformList)
    {
        _entryTransform = Instantiate(_entryTemplate, p_container);
        _entryRectTransform = _entryTransform.GetComponent<RectTransform>();
        _entryRectTransform.anchoredPosition = new Vector2(0f, -_templateHeight * p_transformList.Count);
        _entryTransform.gameObject.SetActive(true);

        _rank = p_transformList.Count + 1;
        _entryTransform.GetChild(0).GetComponent<TMP_Text>().text = _rank.ToString();

        _score = p_highscoreEntry.score;
        _entryTransform.GetChild(1).GetComponent<TMP_Text>().text = _score.ToString();

        p_transformList.Add(_entryTransform);
    }

    public void AddHighscoreEntry(int p_score)
    {
        _highscoreEntry = new HighscoreEntry { score = p_score };

        _jsonString = PlayerPrefs.GetString("highscoreTable");
        _highscores = JsonUtility.FromJson<HighscoresList>(_jsonString);

        _highscores.highscoreEntryList.Add(_highscoreEntry);
        for (int i = 0; i < _highscores.highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < _highscores.highscoreEntryList.Count; j++)
            {
                if (_highscores.highscoreEntryList[j].score > _highscores.highscoreEntryList[i].score)
                {
                    _tmp = _highscores.highscoreEntryList[i];
                    _highscores.highscoreEntryList[i] = _highscores.highscoreEntryList[j];
                    _highscores.highscoreEntryList[j] = _tmp;
                }
            }
            if (i == 10)
            {
                _highscores.highscoreEntryList.Remove(_highscores.highscoreEntryList[i]);
            }
        }
        currentRank = _highscores.highscoreEntryList.IndexOf(_highscoreEntry);

        _json = JsonUtility.ToJson(_highscores);
        PlayerPrefs.SetString("highscoreTable", _json);
        PlayerPrefs.Save();
    }
}