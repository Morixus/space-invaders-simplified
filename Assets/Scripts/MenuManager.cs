using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _statsButton;
    [SerializeField] private Button _closeButton;
    [SerializeField] private GameObject _statsPanel;

    private void Start()
    {
        Time.timeScale = 1f;
        _startButton.onClick.AddListener(() => StartGame());
        _statsButton.onClick.AddListener(() => ShowStats());
        _closeButton.onClick.AddListener(() => CloseStats());
    }

    private void StartGame()
    {
        SceneManager.LoadScene("Game");
    }

    private void ShowStats()
    {
        _statsPanel.SetActive(true);
    }
    private void CloseStats()
    {
        _statsPanel.SetActive(false);
    }
}
