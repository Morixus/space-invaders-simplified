using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject _bulletPrefab = null;
    [SerializeField] private Button _leftButton;
    [SerializeField] private Button _rightButton;
    [SerializeField] private float _speed;
    private Rigidbody2D _rb = null;
    private float _shootCD = 2f;
    private float _lastShot = 0f;
    private Vector3 _bulletSpawnPosition = new Vector3(0f, 0.5f, 0f);
    
    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _lastShot += Time.deltaTime;
        if (_lastShot >= _shootCD)
        {
            Shoot();
        }
    }

    public void Move(float p_horizontalMove)
    {
        _rb.velocity = new Vector2(p_horizontalMove * _speed, _rb.velocity.y);
    }

    private void Shoot()
    {
        Instantiate(_bulletPrefab, this.transform.position + _bulletSpawnPosition, Quaternion.identity);
        _lastShot = 0f;
    }
}