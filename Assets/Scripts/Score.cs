using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    [SerializeField] private TMP_Text _scoreText;
    static protected int _score;
    public int finalScore;

    private void Awake()
    {
        _score = 0;
    }

    private void Update()
    {
        Debug.Log(_score);
        finalScore = _score;
        _scoreText.text = "Points: " + finalScore;
    }
}